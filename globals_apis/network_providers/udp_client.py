import socket
import threading


class UdpClient(threading.Thread):
    """
    UDP message Client
    """

    def __init__(self, udp_ip: str, udp_port: int, buffer_size: int, on_input):
        super().__init__(daemon=True)
        self.udp_ip = udp_ip
        self.udp_port = udp_port
        self.buffer_size = buffer_size
        self._client = self.set_client()
        self.on_input = on_input

    def set_client(self):
        """
        socket client initialization
        """
        _socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        _socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        _socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 8388608)
        _socket.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 8388608)

        _socket.bind((self.udp_ip, self.udp_port))
        return _socket

    def run(self):
        """
        main functionality off reading udp messages
        :return:
        """
        while True:
            data, addr = self._client.recvfrom(self.buffer_size)
            self.on_input(data, addr)
