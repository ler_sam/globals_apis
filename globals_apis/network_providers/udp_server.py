import socket


class UdpServer:
    def __init__(self, udp_ip, udp_port):
        self.udp_ip = udp_ip
        self.udp_port = udp_port
        self._socket = self.set_socket()

    @classmethod
    def set_socket(cls):
        """
        socket initialization
        :return:
        """
        try:
            _socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            _socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            _socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 8388608)
            _socket.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 8388608)

        except ConnectionRefusedError:
            _socket = None

        return _socket

    def send_msg(self, msg):
        """
        sending message to udp
        :param msg:
        """
        if self._socket:
            self._socket.sendto(msg, (self.udp_ip, self.udp_port))
