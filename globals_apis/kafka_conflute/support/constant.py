from enum import Enum


class Compression(Enum):
    GZIP = "gzip"
    SNAPPY = "snappy"
    LZ4 = "lz4"
    ZSTD = "zstd"
    NONE = "none"

    def __str__(self) -> str:
        return self.value


class Serialization(Enum):
    PICKLE = "pickle"
    JSON = "json"
    DEFAULT = "none"

    def __str__(self) -> str:
        return self.value
