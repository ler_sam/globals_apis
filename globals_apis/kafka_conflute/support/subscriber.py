from abc import ABC, abstractmethod
from typing import Callable


class Subscriber(ABC):
    @abstractmethod
    def start(self, on_input: Callable) -> None:
        pass

    @abstractmethod
    def stop(self) -> None:
        pass
