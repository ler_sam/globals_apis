import json
from typing import Union

from confluent_kafka import DeserializingConsumer
from confluent_kafka.serialization import Serializer, SerializationError


class JsonSerializer(Serializer):
    def __call__(self, obj: Union[str, bytes, bytearray], ctx=None):
        if obj is None:
            return None
        try:
            return json.loads(obj)
        except Exception as e:
            raise SerializationError(str(e))


class JsonConsumer(DeserializingConsumer):
    def __init__(self, conf):
        conf['key.deserializer'] = JsonSerializer()
        conf['value.deserializer'] = JsonSerializer()
        super().__init__(conf)
