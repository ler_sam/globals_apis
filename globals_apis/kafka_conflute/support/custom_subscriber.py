import logging
import time
from collections.abc import Callable
from threading import Thread, Event
from confluent_kafka import Consumer, admin, KafkaException, KafkaError
from confluent_kafka.error import ConsumeError

from .constant import Serialization
from .json_consumer import JsonConsumer
from .pickle_consumer import PickleConsumer


class CustomSubscriber(Thread):
    def __init__(self, bootstrap_servers, group_id: str, topic: list, connection_retry: bool = False,
                 serializer: Serialization = Serialization.DEFAULT, configs: dict = None) -> None:
        """
        general kafka consumer

        :str bootstrap_servers:
        :str group_id:
        :str topic: example ["my_topic", "my_topic_prefix.*"]
        :bool connection_retry:
        :Serialization serializer:
        :dict configs:
        """
        super().__init__(name=group_id, daemon=True)
        self.logger = logging.getLogger()
        self.bootstrap_servers = bootstrap_servers
        self.group_id = group_id
        self.topic = topic
        self.on_input = None
        self.connection_retry = connection_retry
        self.serializer = serializer
        self.configs = configs
        self._consumer = self._create_consumer()
        self.is_alive = Event()

    def _create_consumer(self) -> Consumer:
        _config = self.configs if self.configs else {}
        _config["bootstrap.servers"] = self.bootstrap_servers
        _retries = True

        while _retries:
            _retries = self.connection_retry
            try:
                admin.AdminClient(_config).list_topics(timeout=1)  # available broker validation by getting topic list
                _config["group.id"] = self.group_id

                if self.serializer == Serialization.DEFAULT:
                    return Consumer(_config)
                elif self.serializer == Serialization.JSON:
                    return JsonConsumer(_config)
                elif self.serializer == Serialization.PICKLE:
                    return PickleConsumer(_config)

            except KafkaException as e:
                self.logger.warning("Attempting to start kafka-publisher. Waiting 10 seconds for available broker...")
                time.sleep(10)
                continue

    def run(self):
        if not self.on_input:
            raise Exception("Missing OnInput Callable")
        self.is_alive.set()

        self._consumer.subscribe(self.topic)
        while self.is_alive.is_set():
            try:
                msg = self._consumer.poll(1.0)

                if msg is None:
                    continue
                elif msg.error():
                    if msg.error().code() == KafkaError.UNKNOWN_TOPIC_OR_PART:
                        logging.warning("Received unknown topic: %s", msg.topic())
                # key = msg.key()
                received_topic = msg.topic()
                value = msg.value()

                self.on_input(received_topic, value)
            except ConsumeError as e:
                if e.code == KafkaError.UNKNOWN_TOPIC_OR_PART:
                    logging.warning('Received unknown topic: %s', self.topic)

    def set_callback(self, callback: Callable) -> None:
        self.on_input = callback

    def join(self, timeout=None):
        self.is_alive.clear()
        self._consumer.close()
        super().join()
