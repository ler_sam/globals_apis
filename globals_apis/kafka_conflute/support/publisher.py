from abc import ABC, abstractmethod


class Publisher(ABC):
    @abstractmethod
    def send(self, msg, dest=None):
        pass

    @abstractmethod
    def stop(self):
        pass
