import logging
import time
from confluent_kafka import Producer, admin, KafkaException
from .constant import Serialization, Compression
from .json_producer import JsonProducer
from .pickle_producer import PickleProducer


class CustomPublisher:
    def __init__(self, bootstrap_servers: str, connection_retry: bool = False,
                 serializer: Serialization = Serialization.DEFAULT, configs: dict = None):
        self.logger = logging.getLogger()
        self.bootstrap_servers = bootstrap_servers
        self.connection_retry = connection_retry
        self.serializer = serializer
        self.configs = configs if configs else {}
        self._producer = self._create_producer()

    def _create_producer(self) -> Producer:
        _config = self.configs if self.configs else {}

        _config["bootstrap.servers"] = self.bootstrap_servers
        _config["compression.type"] = Compression.GZIP
        _retries = True
        while _retries:
            _retries = self.connection_retry
            try:
                admin.AdminClient(_config).list_topics(timeout=1)  # available broker validation by getting topic list

                if self.serializer == Serialization.DEFAULT:
                    return Producer(_config)
                elif self.serializer == Serialization.JSON:
                    return JsonProducer(_config)
                elif self.serializer == Serialization.PICKLE:
                    return PickleProducer(_config)

            except KafkaException as e:
                self.logger.warning("Attempting to start kafka-publisher. Waiting 10 seconds for available broker...")
                time.sleep(10)
                continue

    def send(self, topic, value):
        self._producer.produce(topic=topic, value=value)

    def flush(self):
        self._producer.flush()

    def close(self):
        if self._producer:
            self._producer.flush()

    def __del__(self):
        self.close()
        self._producer = None
