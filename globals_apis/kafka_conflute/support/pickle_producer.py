import pickle
from confluent_kafka import SerializingProducer
from confluent_kafka.serialization import Serializer, SerializationError


class PickleSerializer(Serializer):
    def __call__(self, obj, ctx=None):
        if obj is None:
            return None
        try:
            return pickle.dumps(obj)
        except Exception as e:
            raise SerializationError(str(e))


class PickleProducer(SerializingProducer):
    def __init__(self, conf):
        conf['key.serializer'] = PickleSerializer()
        conf['value.serializer'] = PickleSerializer()
        super().__init__(conf)
