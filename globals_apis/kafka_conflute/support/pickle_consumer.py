import pickle
from typing import Union

from confluent_kafka import DeserializingConsumer
from confluent_kafka.serialization import Serializer, SerializationError


class PickleSerializer(Serializer):
    def __call__(self, obj: Union[bytes, bytearray], ctx=None):
        if obj is None:
            return None
        try:
            return pickle.loads(obj)
        except Exception as e:
            raise SerializationError(str(e))


class PickleConsumer(DeserializingConsumer):
    def __init__(self, conf):
        conf['key.deserializer'] = PickleSerializer()
        conf['value.deserializer'] = PickleSerializer()
        super().__init__(conf)
