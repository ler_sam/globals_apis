import json
from confluent_kafka import SerializingProducer
from confluent_kafka.serialization import Serializer, SerializationError


class JsonSerializer(Serializer):
    def __call__(self, obj, ctx=None):
        if obj is None:
            return None
        try:
            return json.dumps(obj)
        except Exception as e:
            raise SerializationError(str(e))


class JsonProducer(SerializingProducer):
    def __init__(self, conf):
        conf['key.serializer'] = JsonSerializer()
        conf['value.serializer'] = JsonSerializer()
        super().__init__(conf)
