import logging

from .support import CustomPublisher, Serialization, Publisher


class KafkaPublisher(Publisher):

    def __init__(self, bootstrap_servers, connection_retry=False,
                 serializer: Serialization = Serialization.DEFAULT, **configs):
        self.logger = logging.getLogger()
        self.producer = CustomPublisher(bootstrap_servers=bootstrap_servers, connection_retry=connection_retry,
                                        serializer=serializer, configs=configs)
        if not hasattr(self, "producer"):
            raise Exception(
                "Kafka initialization failure. Please verify the Kafka server is running and accessible, "
                "then try again.")

    def send(self, msg, dest=None):
        """
        sending message to destination topic
        :param msg:
        :param dest:
        :return:
        """
        if dest is None:
            raise ValueError("dest parameter cannot be None")

        self.producer.send(topic=dest, value=msg)

    def flush(self):
        """Waits for any outstanding messages to be delivered."""
        if hasattr(self, "producer") and self.producer:
            self.producer.flush()

    def __del__(self):
        self.stop()

    def stop(self):
        self.flush()
        if hasattr(self, "producer"):
            self.producer.close()
            del self.producer
