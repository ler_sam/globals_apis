[[_TOC_]]

# Kafka Confluent

## support links

[confluent-kafka-python](https://docs.confluent.io/platform/current/clients/confluent-kafka-python/html/index.html)
[Confluent Kafka API](https://docs.confluent.io/platform/current/clients/confluent-kafka-python/html/index.html)

## YouTube

[Intro to Confluent-Kafka Python | Apache Kafka® for Python Developers](https://www.youtube.com/playlist?list=PLa7VYi0yPIH1odVnZC430071CVD_4Sx1e)

[Apache Kafka Tutorials | Kafka 101](https://www.youtube.com/playlist?list=PLa7VYi0yPIH0KbnJQcMv5N9iW8HkZHztH)
# AdminClient
## Configuration

**_documentation_
**: [admin-configs](https://docs.confluent.io/platform/current/installation/configuration/admin-configs.html)


# Producer

## Configuration

**_documentation_
**: [producer-configs](https://docs.confluent.io/platform/current/installation/configuration/producer-configs.html)

| Config Name                                                                                                                            | Description                                                                                                            | Default Value       |
|:---------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------|:--------------------|
| [bootstrap.servers](https://docs.confluent.io/platform/current/installation/configuration/producer-configs.html#bootstrap-servers)     | A list of host/port pairs to use for establishing the initial connection to the Kafka cluster.                         | ""                  |
| [acks](https://docs.confluent.io/platform/current/installation/configuration/producer-configs.html#acks)                               | Level of acknowledgment required before returning from produce request.                                                | all                 |
| [batch.size](https://docs.confluent.io/platform/current/installation/configuration/producer-configs.html#batch-size)                   | Number of bytes that will be batched before sending produce request.                                                   | 16,384              |
| [linger.ms](https://docs.confluent.io/platform/current/installation/configuration/producer-configs.html#linger-ms)                     | Number of milliseconds to wait for batch before sending produce request.                                               | 0                   |
| [compression.type](https://docs.confluent.io/platform/current/installation/configuration/producer-configs.html#compression-type)       | Algorithm used to compress data sent to broker. (none, gzip, snappy, lz4 or zstd)                                      | none                |
| [retries](https://docs.confluent.io/platform/current/installation/configuration/producer-configs.html#retries)                         | Number of times to retry a request that failed for potentially transient reasons.                                      | MAXINT              |
| [delivery.timeout.ms](https://docs.confluent.io/platform/current/installation/configuration/producer-configs.html#delivery-timeout-ms) | Time Limit for overall produce request. Can also be used to limit retries.                                             | 120,000 (2 minutes) |
| [transactional.id](https://docs.confluent.io/platform/current/installation/configuration/producer-configs.html#transactional-id)       | A unique id for the producer that enables transaction recovery across multiple sessions of a single producer instance. | None                |
| [enable.idempotence](https://docs.confluent.io/platform/current/installation/configuration/producer-configs.html#enable-idempotence)   | When set to ‘true’, the producer will ensure that exactly one copy of each message is written in the stream.           | True                |

## Compression Algorithm
https://quix.io/blog/how-to-use-gzip-data-compression-with-apache-kafka-and-python

### gzip

### Snappy
[wikipedia](https://en.wikipedia.org/wiki/Snappy_(compression)#cite_note-5)
Snappy (previously known as Zippy) is a fast data compression and decompression library written in C++ by Google based on ideas from LZ77 and open-sourced in 2011.[3][4] It does not aim for maximum compression, or compatibility with any other compression library; instead, it aims for very high speeds and reasonable compression. Compression speed is 250 MB/s and decompression speed is 500 MB/s using a single core of a circa 2011 "Westmere" 2.26 GHz Core i7 processor running in 64-bit mode. The compression ratio is 20–100% lower than gzip.


# Consumer

## Configuration

**_documentation_
**: [consumer-configs](https://docs.confluent.io/platform/current/installation/configuration/consumer-configs.html)

| Config Name                                                                                                                          | Description                                                                                  | Default Value    |
|:-------------------------------------------------------------------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------|:-----------------|
| [bootstrap.servers](https://docs.confluent.io/platform/current/installation/configuration/consumer-configs.html#bootstrap-servers)   | list of host/port pairs to use for establishing the initial connection to the Kafka cluster. | ""               |
| [group.id](https://docs.confluent.io/platform/current/installation/configuration/consumer-configs.html#group-id)                     | A unique string that identifies the consumer group this consumer belongs to.                 | Null             |
| [auto.offset.reset](https://docs.confluent.io/platform/current/installation/configuration/consumer-configs.html#auto-offset-reset)   | Determines offset to begin consuming at if no valid stored offset is available               | latest           |
| [enable.auto.commit](https://docs.confluent.io/platform/current/installation/configuration/consumer-configs.html#enable-auto-commit) | If true, periodically commit offsets in the background                                       | true             |
| [isolation.level](https://docs.confluent.io/platform/current/installation/configuration/consumer-configs.html#isolation-level)       | Used in transactional processing.                                                            | read_uncommitted |


































