import uuid

from typing import Callable
from .support import Serialization, CustomSubscriber, Subscriber


class KafkaSubscriber(Subscriber):

    def __init__(self, bootstrap_servers, topic: list, group_id: str = None, connection_retry=False,
                 serializer: Serialization = Serialization.DEFAULT, **configs):
        group_id = group_id if group_id else str(uuid.uuid4())
        self.bootstrap_servers = bootstrap_servers
        self.topic = topic
        self.group_id = group_id
        self.connection_retry = connection_retry
        self.serializer = serializer
        self.configs = configs
        self.on_input = None
        self.subscriber = CustomSubscriber(bootstrap_servers=bootstrap_servers, group_id=group_id, topic=topic,
                                           connection_retry=connection_retry, serializer=serializer, configs=configs)
        if not hasattr(self, "subscriber"):
            raise Exception(
                "Kafka initialization failure. Please verify the Kafka server is running and accessible, "
                "then try again.")

    def start(self, on_input: Callable) -> None:
        self.on_input = on_input
        self.subscriber.set_callback(on_input)
        self.subscriber.start()

    def stop(self) -> None:
        self.subscriber.join()

    def change_subscription(self, new_topic: list) -> None:
        self.stop()
        self.subscriber = None
        self.subscriber = CustomSubscriber(bootstrap_servers=self.bootstrap_servers, group_id=self.group_id,
                                           topic=new_topic, connection_retry=self.connection_retry,
                                           serializer=self.serializer, configs=self.configs)
        self.start(on_input=self.on_input)
