import string
import random


def letters_digits_string(str_length: int) -> str:
    """
    generate string random string with selected length
    :param str_length: required string length
    :return: random string with selected length
    """
    letters = string.ascii_letters + string.digits
    return ''.join(random.choice(letters) for _ in range(str_length))
