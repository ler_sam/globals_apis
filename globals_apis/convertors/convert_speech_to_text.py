import whisper


def convert_speech_to_text(video_path: str) -> str:
    """
    conberting speech to text
    :param video_path:
    :return: retrieved text
    """
    model = whisper.load_model(name="base", in_memory=True)
    result = model.transcribe(video_path, fp16=False)
    return result['text']
