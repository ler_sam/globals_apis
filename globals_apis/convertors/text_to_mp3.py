#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
https://pypi.org/project/gTTS/
https://gtts.readthedocs.io/en/latest/index.html
"""

from gtts import gTTS, lang


def get_support_languages():
    return lang.tts_langs()


def text_to_mp3(file_data: str, mp_path: str, user_lang='en'):
    tts = gTTS(file_data, lang=user_lang)
    tts.save(f"{mp_path}.mp3")
