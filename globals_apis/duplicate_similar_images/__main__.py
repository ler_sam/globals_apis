# Inspired from https://github.com/JohannesBuchner/imagehash repository
import os
import shutil

import imagehash
import numpy as np
from PIL import Image

from globals_apis.application import logger


def alpha_remover(image):
    if image.mode != 'RGBA':
        return image
    canvas = Image.new('RGBA', image.size, (255, 255, 255, 255))
    canvas.paste(image, mask=image)
    return canvas.convert('RGB')


def with_ztransform_preprocess(hashfunc, hash_size=8):
    def function(path):
        image = alpha_remover(Image.open(path))
        image = image.convert("L").resize((hash_size, hash_size), Image.ANTIALIAS)
        data = image.getdata()
        quantiles = np.arange(100)
        quantiles_values = np.percentile(data, quantiles)
        zdata = (np.interp(data, quantiles_values, quantiles) / 100 * 255).astype(np.uint8)
        image.putdata(zdata)
        return hashfunc(image)

    return function


def filter_img_links(img_dir: str) -> list:
    included_extensions = ['.jpg', '.jpeg', '.bmp', '.png', '.gif']
    img_path = []
    for root, _, files in os.walk(img_dir):
        for file in files:
            # check the extension of files
            if any(file.endswith(ext) for ext in included_extensions):
                img_path.append(os.path.join(root, file))

    return img_path


if __name__ == '__main__':
    _img_dir = '/run/user/1000/gvfs/smb-share:server=192.168.1.106,share=samuel/Pictures'
    _duplicate = '/tmp'
    img_paths = filter_img_links(_img_dir)

    img_mapper = {}
    dhash_z_transformed = with_ztransform_preprocess(imagehash.dhash, hash_size=8)
    for img_f in img_paths:
        try:
            img_key = dhash_z_transformed(img_f)
            if img_key not in img_mapper:
                img_mapper[img_key] = img_f
            else:
                img_f, img_mapper[img_key] = img_mapper[img_key], img_f
                logger.info('duplicate: %s; moved: %s', img_mapper[img_key], img_f)
                logger.info('moved from: %s', img_f)
                shutil.move(img_f, _duplicate)
                logger.info('------------------------------')
        except FileNotFoundError:
            logger.error('file not found %s',img_f)
        except shutil.Error as err:
            logger.error(err)
        except OSError as os_err:
            logger.error(os_err)
    logger.info('done')
