from __future__ import print_function
# https://developers.google.com/calendar/api/v3/reference/events
import datetime
import os.path
from typing import Union

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


def set_credentials(scopes: list) -> Credentials:
    """
    credentials.json - take from
    https://console.cloud.google.com/apis/credentials?authuser=1&project=industrial-glow-397408&supportedpurview=project
    :return:
    """
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', scopes)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file('credentials.json', scopes)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())
    return creds


def get_event_by_query(creds: Credentials, query: str) -> Union[dict, None]:
    """
    retrieve old events by query string
    :param query:
    :return:
    """

    try:
        service = build('calendar', 'v3', credentials=creds)

        # Call the Calendar API
        now = datetime.datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time
        events_result = service.events().list(calendarId='primary', q=query, timeMax=now, singleEvents=True,
                                              orderBy='startTime').execute()

        events = events_result.get('items', [])

        if events:
            return events

    except HttpError as error:
        print('An error occurred: %s' % error)

    return None


def delete_events_by_ids(event_ids: list):
    try:
        service = build('calendar', 'v3', credentials=creds)
        for event_id in event_ids:
            print(event_id)
            service.events().delete(calendarId='primary', eventId=event_id).execute()

    except HttpError as error:
        print('An error occurred: %s' % error)


if __name__ == '__main__':
    # If modifying these scopes, delete the file token.json.
    scopes = ['https://www.googleapis.com/auth/calendar']
    creds = set_credentials(scopes)

    events_list = get_event_by_query(creds=creds, query="רופא שיניים")
    if events_list:
        event_ids = [event['id'] for event in events_list]
        delete_events_by_ids(event_ids)

    print(events_list)
