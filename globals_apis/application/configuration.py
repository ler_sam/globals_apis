import os
import logging.config

from dotenv import load_dotenv
from .settings import LOGGER_SETTINGS

load_dotenv ()
LOG_TYPE = os.environ.get("LOG_TYPE", "production")

logging.config.dictConfig(LOGGER_SETTINGS)
# logger = logging.getLogger('automation')
# logger = logging.getLogger('production')
logger = logging.getLogger(LOG_TYPE)