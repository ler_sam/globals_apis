from .logger_formatters import JSONFormatter

LOGGER_SETTINGS = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(asctime)s %(message)s'
        },
        'json': {
            "()": JSONFormatter
        }
    },
    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
            'formatter': 'simple'
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'file': {
            'class': 'logging.FileHandler',
            'filename': '/tmp/automation.log',
            'formatter': 'verbose',
        },
        'temp': {
            'formatter': 'json',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout'
        }
    },
    'loggers': {
        'automation': {
            'level': 'DEBUG',
            'handlers': ['file', 'console'],
            'propagate': True
        },
        'production': {
            'level': 'INFO',
            'handlers': ['file'],
            'propagate': True
        },
        'devops': {
            'level': 'DEBUG',
            'handlers': ['file', 'temp'],
            'propagate': True
        }
    }
}
