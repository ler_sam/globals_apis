import time

from kafka import (KafkaConsumer, errors)


class KafkaSubscriber:
    def __init__(self, **configs):
        self._consumer = None
        con_status = False
        retry_counter = 0
        while not con_status and retry_counter < 2:
            try:
                self._consumer = KafkaConsumer(**configs)
                con_status = self._consumer.bootstrap_connected()
            except errors.NoBrokersAvailable:
                time.sleep(5)
                retry_counter += 1
        if self._consumer is None:
            raise errors.NoBrokersAvailable()

    def set_topics(self, topics: list = None, pattern: str = None):
        """
        topics (list): List of topics for subscription.
        pattern (str): Pattern to match available topics. You must provide either topics or pattern, but not both.
        """
        if topics:
            self._consumer.subscribe(topics=topics)
        elif pattern:
            self._consumer.subscribe(pattern=pattern)
        else:
            raise ValueError(f"one of parameters is required topics={topics} or pattern={pattern}")

    def change_topic(self, topics: list = None, pattern: str = None):
        self._consumer.unsubscribe()
        self.disconnect(autocommit=True)
        self.set_topics(topics, pattern)

    def disconnect(self, autocommit=False):
        if self._consumer:
            self._consumer.close(autocommit)
