import time

from kafka import (KafkaProducer, errors)


class KafkaPublisher:
    def __init__(self, **configs):
        self._producer = None
        con_status = False
        retry_counter = 0

        while not con_status and retry_counter < 2:
            try:
                self._producer = KafkaProducer(**configs)
                con_status = self._producer.bootstrap_connected()
            except errors.NoBrokersAvailable:
                time.sleep(5)
                retry_counter += 1
        if self._producer is None:
            raise errors.NoBrokersAvailable()

    def error_callback(self, error_data):
        print(error_data)

    def send(self, topic, message):
        """
        topic – topic where the message will be published
        message – Must be type bytes, or be serializable to bytes via configured value_serializer.
                If value is None, key is required and message acts as a 'delete'.
                See kafka-python compaction documentation for more details:
                    https://kafka.apache.org/documentation.html#compaction (compaction requires kafka-python >= 0.8.1)
        """
        return self._producer.send(topic=topic, value=message).add_errback(self.error_callback)

    def __del__(self):
        if self._producer:
            self._producer.close(timeout=2)
