from typing import Optional
from pytube import YouTube


def download_youtube_audio(video_path: str, output_path: Optional[str] = None) -> str:
    """
    Getting audio from YouTube files
    :param video_path: YouTube link
    :param output_path: output path directory
    :return: path to download file path
    """
    data = YouTube(video_path)

    audio = data.streams.get_audio_only()
    return audio.download(output_path=output_path)


def download_youtube(video_path: str, output_path: str) -> str:
    """
    download YouTube movie
    :param video_path: YouTube link
    :param output_path: output directory path
    :return:
    """
    data = YouTube(video_path)
    movie = data.streams.get_highest_resolution()
    movie.download(output_path=output_path)
