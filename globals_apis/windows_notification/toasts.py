from win10toast import ToastNotifier


def display_windows_toasts(toast_title: str, toast_message: str, toast_duration: int, icon_path=None) -> bool:
    toaster = ToastNotifier()
    toaster.show_toast(toast_title, toast_message, threaded=True, icon_path=icon_path, duration=toast_duration)
    return toaster.notification_active()
