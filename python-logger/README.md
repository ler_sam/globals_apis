[TOC]
all loggers in python logging library are Singleton Objects.

# Log Level Filters

| Level        | value | when it's used                                                                                                                                                                                                         |
|:-------------|:-----:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **NOT SET**  |   0   |                                                                                                                                                                                                                        |
| **DEBUG**    |  10   | Detailed information, typically of interest only when diagnosing problems.                                                                                                                                             |
| **INFO**     |  20   | General information about the application's progress.                                                                                                                                                                  |
| **WARNING**  |  30   | Potentially problematic situations.<br/>An indication that something unexpected happened, or indicative of some problem in the near future<br/>(e.g. ‘disk space low’).<br/>The software is still working as expected. |
| **ERROR**    |  40   | Errors that do not prevent the application from continuing.<br/>Due to a more serious problem, the software has not been able to perform some function.                                                                |   
| **CRITICAL** |  50   | A serious error, indicating that the program itself may be unable to continue running.                                                                                                                                 | 

The default level is **WARNING**, which means that only events of this severity and higher will be tracked, unless the
logging package is configured to do otherwise.

Configure a logger (or individual handler) to same log level X, it makes shor that record level >= X will be process, otherwise skip/
example:

![log_level.png](images/log_level.png)

# Handlers and Formatters

## Handlers

determine where your log messages will be sent.

**Python offers several built-in handlers:**

- StreamHandler: Sends log messages to standard output (console) or a file-like object.
- FileHandler: Writes log messages to a specified file.
- RotatingFileHandler: Creates new log files after a certain size or time interval.
- TimedRotatingFileHandler: Creates new log files-based on time intervals (e.g., hourly, daily).
- SMTPHandler: Sends log messages via email.
- SysLogHandler: Sends log messages to the system's syslog facility.
- can write your own custom handlers

## Formatters

control the appearance of your log messages. You can customize the output by using format specifiers.
Formater attach to Handler

**Common format specifiers:**

- %(asctime)s: Timestamp of the log record.
- %(name)s: Name of the logger.
- %(levelname)s: Log level (e.g., DEBUG, INFO, WARNING).
- %(message)s: Log message content.
- %(filename)s: Filename of the source code.
- %(funcName)s: Name of the function where the log message originated.
- %(lineno)d: Line number in the source code.

# Logger with Handler and Formater

![Logger with Handler](images/multiple_handlers.png)
Logger received log request and creates log record with data we send and additional data like date, time, module and
more.

Than this information sends to **Handler module**. Handler have formater so format doesn't happening on logger level,
Formating happens in Handler.

Handler does serialization, formating, all transformation to another python object and send to receiver/ target

# Multiple Handlers

logger can have multiple handlers (and their associated formatter). That why we don't associate formater with logger,
because logger can have multiple handlers, so handler should have his own formatter.

![Multiple Handlers](images/multiple_handlers.png)

- single logger can send the same log record to multiple destinations
- formatter is tied to handler, so each handler can have its own formatter
- example:
    - send all logs to console, send only ERROR and CRITICAL to HTTP

# Filters

- Filters provide a finer grained facility for determining which log records to output.
- Filters like "rules" that determine whether a handler should process the log, or just skip it
    - can be attached to logger itself
        - applies to all handlers attached to the logger
    - can be attached to a specific handler within the logger
        - applies to the handler only
        - each handler can have different filter
    - two filters types
        - based on log level
        - custom filter
            - actually defined using a **Filter** Class
            - basically a predicate function that inspects the data in the log record<br /> and returns True(process
              record) or False (skip processing)
## Log Level Filters
| value | Level    |
|:-----:|:---------|
|  50   | CRITICAL |
|  40   | ERROR    |
|  30   | WARNING  |
|  20   | INFO     |
|  10   | DEBUG    |
|   0   | NOT SET  |

**example:**
![Multiple Handlers](images/filers_log_level.png)

In this case I set logger level to WARNING.
I have two handlers
1. level DEBUG
2. level WARNING

What would happen logger itself wil handle the first filtering and not process all messages under WARNING level.

So in handler 1 even if log level is DEBUG, it will never receive log level debug, because log was filtered out

**So effective log level of this logger is WARNING**

when you have a message string this is why info right message if I had interpolated data have incurred the cost of doing that string interpolation.
I'm issuing the command, doing by using the other syntax.

<span style="color: green;">
By this format logger.info("format", parameters) you're letting the logger do the interpolation,
so what it's going to do is that it is discounting the log request, it is not going to do the interpolation, 
so you've saved some time you've saved some overhead of not doing the interpolation.
</span>
<p>
<span style="color: blue; font-weight: bold;">
So that's why the recommendation is not to do interpolation ahead of time, because you might be incurring it for no reason
</span>
</p>

# Logger Hierarchy
organize in tree hierarchy

- **Root Loger** - special logger that created by default in Python's logging library. It has no handlers by default, but it can propagate log records to parent loggers. Always the "top most" logger.
- tree hierarchy is determined by the logger **names**, based on dot _notation_
![Multiple Handlers](images/Hierarchy.png)
- the names of loggers are used to determine the hierarchy


# Common Pattern
- applies to setting up logging for your apps, not libraries
- configure root logger to emit your logs via one or more handlers
- use a single named logger for all logging
- use named loggers for "sections" of your application
- use named loggers for each module: getLogger(__name__)

# Security Risks
Treat your logs as if they were going to be public!

-  Do not log password, credential or sensitive information

**rely on:**

[Python Logging Demystified: Part 1 - Concepts](https://www.youtube.com/watch?v=AXUrQ-SexMI&list=PLopqU1yGr5fp895paT460cQsBU8vZVsds)

[Logging HOWTO](https://docs.python.org/3/howto/logging.html#)
