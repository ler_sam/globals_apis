import argparse

from globals_apis.windows_notification import display_windows_toasts

parser = argparse.ArgumentParser('Windows toast simple notification')
parser.add_argument("-t", "--title", type=str, help="Notification Title")

parser.add_argument("-m", "--message", type=str, help="Notification Message")
parser.add_argument("-d", "--duration", type=int, help="Notification Duration")

if __name__ == '__main__':
    args = parser.parse_args()
    title = args.title or None
    message = args.message or None
    duration = args.duration or None

    if title and message and duration:
        status = display_windows_toasts(toast_title=title, toast_message=message, toast_duration=duration)
        print(status)
