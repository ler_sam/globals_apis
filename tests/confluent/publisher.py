import random
import time

from globals_apis.generators import letters_digits_string
from globals_apis.kafka_conflute import KafkaPublisher
from globals_apis.kafka_conflute.support import Serialization

if __name__ == '__main__':
    topic = "samuel"
    bootstrap_servers = "localhost:9092"
    configs = {
        # 'sasl.username': '<CLUSTER API KEY>',
        # 'sasl.password': '<CLUSTER API SECRET>',
        #
        # # Fixed properties
        # 'security.protocol': 'SASL_SSL',
        # 'sasl.mechanism': 'PLAIN',
        # 'group.id': 'kafka-python-getting-started',
        # 'auto.offset.reset': 'earliest'
    }
    publisher = KafkaPublisher(bootstrap_servers, connection_retry=True, serializer=Serialization.PICKLE, **configs)
    for _ in range(5):
        msg = {"data": letters_digits_string(random.randint(1, 30))}
        publisher.send(msg=msg, dest=topic)
        time.sleep(1)
    publisher.flush()
    print("--- Done ---")
