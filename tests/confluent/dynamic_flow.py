import random
import time

from globals_apis.generators import letters_digits_string
from globals_apis.kafka_conflute import KafkaPublisher, KafkaSubscriber
from globals_apis.kafka_conflute.support import Serialization


def on_input(key, value):
    print(key, value)


if __name__ == '__main__':
    topic = ["samuel", "second"]
    group_id = "lersam"
    bootstrap_servers = "localhost:9092"
    publisher = KafkaPublisher(bootstrap_servers, connection_retry=True, serializer=Serialization.PICKLE)
    client = KafkaSubscriber(bootstrap_servers=bootstrap_servers, topic=topic, group_id=group_id,
                             serializer=Serialization.PICKLE)
    client.start(on_input=on_input)
    for _ in range(5):
        msg = {"data": letters_digits_string(random.randint(1, 30))}
        publisher.send(msg=msg, dest=topic[0])
        time.sleep(1)
    publisher.flush()

    for _ in range(5):
        msg = {"data": letters_digits_string(random.randint(1, 30))}
        publisher.send(msg=msg, dest=topic[1])
        time.sleep(1)

    publisher.stop()
    client.stop()


