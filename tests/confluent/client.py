import time

from globals_apis.kafka_conflute import KafkaSubscriber
from globals_apis.kafka_conflute.support import Serialization


def on_input(key, value):
    print(key, value)


if __name__ == '__main__':
    topic = ["samuel"]
    group_id = "lersam"
    bootstrap_servers = "localhost:9092"

    configs = {
        # 'sasl.username': '<CLUSTER API KEY>',
        # 'sasl.password': '<CLUSTER API SECRET>',
        #
        # # Fixed properties
        # 'security.protocol': 'SASL_SSL',
        # 'sasl.mechanism': 'PLAIN',
        #
        'auto.offset.reset': 'earliest'
    }

    client = KafkaSubscriber(bootstrap_servers=bootstrap_servers, topic=topic, group_id=group_id,
                             serializer=Serialization.PICKLE, **configs)
    client.start(on_input=on_input)
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        client.stop()
