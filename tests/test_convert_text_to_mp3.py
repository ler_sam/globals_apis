from globals_apis.convertors import text_to_mp3, get_support_languages


def execute_convert():
    file_path = 'ext/input_file.txt'

    input_text = ''
    with open(file_path, 'r', encoding='utf-8') as input_file:
        input_text = input_file.read()

    text_to_mp3(input_text, 'ext/result', 'ru')

    print('finish')


def support_languages():
    print(get_support_languages())


if __name__ == "__main__":
    support_languages()
