import argparse

from globals_apis.convertors import convert_speech_to_text
from globals_apis.general import download_youtube_audio, download_youtube

parser = argparse.ArgumentParser('Convert YouTube videos sound truck to text file')
parser.add_argument("--video_id", type=str, help="YouTube video id")


def speech_to_text(video_path: str, text_path: str) -> str:
    file_path = download_youtube_audio(video_path, 'ext')

    video_txt = convert_speech_to_text(file_path)

    with open(text_path, 'w') as f:
        f.write(video_txt)
    return video_txt


if __name__ == '__main__':
    args = parser.parse_args()
    video_id = args.video_id or None
    if video_id is None:
        raise ValueError(f'incorrect video_id parameter: {video_id}')

    src_path = f"https://www.youtube.com/watch?v={video_id}"
    dest_path = f'ext/{video_id}.txt'

    download_youtube(src_path, 'ext')

    # speech_to_text(src_path, dest_path)
