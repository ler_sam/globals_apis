import uvicorn
from typing import Annotated

from fastapi import FastAPI, Depends
from starlette import status

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session

from task import process_data

# --- Database Configuration ---
SQLALCHEMY_DATABASE_URL = "mysql+pymysql://admin:Anutk1976@127.0.0.1:3306/TodoApplication"
engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


# --- Dependency Injection for Database Session ---
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


# --- FastAPI Application ---
app = FastAPI()
db_dependency = Annotated[Session, Depends(get_db)]


@app.post("/data", status_code=status.HTTP_200_OK)
async def create_data(data: dict, db: db_dependency):
    process_data.delay(data)  # Pass any necessary data to the task

    return {"message": "Data received and processing scheduled"}


if __name__ == "__main__":
    uvicorn.run("main:app", port=8080, host="0.0.0.0", reload=True)
