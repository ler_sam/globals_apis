from celery import Celery
from celery.schedules import crontab

# --- Celery Configuration ---
celery_app = Celery('your_project_name',
                    broker='sqla+sqlite:///db.sqlite3',
                    backend='db+sqlite:///db.sqlite3')


# --- Celery Task ---
@celery_app.task
def process_data(data):
    print("called: ", data)
