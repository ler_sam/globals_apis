- [FastAPI and celery](https://levelup.gitconnected.com/running-fastapi-and-celery-together-in-a-single-command-66afd9b15561)
- [FastAPI and celery](https://testdriven.io/blog/fastapi-and-celery/)

# Execute Celery Tasks

```commandline
celery -A task worker --loglevel=DEBUG
```

```commandline
celery -A task worker --loglevel=INFO
```