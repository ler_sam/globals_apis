import uvicorn

from fastapi import FastAPI
from routers import maintenance

app = FastAPI()
app.include_router(maintenance.router)

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8080, reload=True)
