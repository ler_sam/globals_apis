from fastapi import APIRouter
from starlette import status
from database import client

router = APIRouter(
    prefix="/maintenance",
    tags=["maintenance"]
)


@router.get("/collections", status_code=status.HTTP_200_OK)
async def get_collections(database_name: str):
    return client[database_name].list_collection_names()


@router.get("/backup", status_code=status.HTTP_200_OK)
async def backup_database(collection: str, start_time: int = -1, end_time: int = -1):
    pass


@router.get("/restore", status_code=status.HTTP_200_OK)
async def restore_database():
    pass
