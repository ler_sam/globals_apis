import os
import time

from dotenv import load_dotenv

from threading import Event

from globals_apis.kafka_conflute import KafkaSubscriber

load_dotenv()


def on_input(topic, mssg):
    print(topic, mssg)


def kafka_subscriber(is_active: Event):
    _topic = os.environ.get('TESTING_TOPIC')
    _servers = os.environ.get('BOOTSTRAP_SERVERS')
    consumer = KafkaSubscriber(bootstrap_servers=_servers, group_id="favorite_group", topic=_topic)
    consumer.start(on_input)
    # 'my_favorite_topic'

    while not is_active.is_set():
        time.sleep(30)

    consumer.stop()


if __name__ == '__main__':
    _active = Event()
    try:
        kafka_subscriber(_active)
    except KeyboardInterrupt:
        _active.set()
