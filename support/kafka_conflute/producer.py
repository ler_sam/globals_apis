import os

from dotenv import load_dotenv

from threading import Event

from globals_apis.kafka_conflute import KafkaPublisher

load_dotenv()


def kafka_producer(is_active: Event):
    _topic = os.environ.get('TESTING_TOPIC')
    _servers = os.environ.get('BOOTSTRAP_SERVERS')
    producer = KafkaPublisher(bootstrap_servers=_servers)
    for _ in range(100):
        producer.send(_topic, b'some_message_bytes')

    is_active.set()


if __name__ == '__main__':
    _active = Event()
    try:
        while not _active.is_set():
            kafka_producer(_active)
    except KeyboardInterrupt:
        _active.set()
