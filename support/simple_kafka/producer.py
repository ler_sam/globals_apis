import os

from dotenv import load_dotenv
from kafka import KafkaProducer
from threading import Event

load_dotenv()


def kafka_producer(is_active: Event):
    _topic = os.environ.get('TESTING_TOPIC')
    _servers = os.environ.get('BOOTSTRAP_SERVERS')
    producer = KafkaProducer(bootstrap_servers=_servers)
    for _ in range(100):
        future = producer.send(_topic, b'some_message_bytes')
        result = future.get(timeout=60)
        print(result)

    producer.flush()
    is_active.set()


if __name__ == '__main__':
    _active = Event()
    try:
        while not _active.is_set():
            kafka_producer(_active)
    except KeyboardInterrupt:
        _active.set()
