import os
import sys

from dotenv import load_dotenv

from globals_apis.simple_kafka import KafkaPublisher
from threading import Event

load_dotenv()


def error_callback(error_data):
    print(error_data)
    sys.exit(1)


def kafka_producer(is_active: Event):
    _topic = os.environ.get('TESTING_TOPIC')
    _servers = os.environ.get('BOOTSTRAP_SERVERS')
    producer = KafkaPublisher(bootstrap_servers=_servers)
    while not is_active.is_set():
        future = producer.send(_topic, b'some_message_bytes')
        future.add_errback(error_callback)


if __name__ == '__main__':
    is_active = Event()
    try:
        kafka_producer(is_active)
    except KeyError:
        is_active.set()
