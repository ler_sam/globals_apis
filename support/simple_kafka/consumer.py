import os
from dotenv import load_dotenv
from kafka import KafkaConsumer
from threading import Event

load_dotenv()


def kafka_subscriber(is_active: Event):
    _topic = os.environ.get('TESTING_TOPIC')
    _servers = os.environ.get('BOOTSTRAP_SERVERS')
    consumer = KafkaConsumer(bootstrap_servers=_servers, group_id="favorite_group")
    consumer.subscribe([_topic])
    # 'my_favorite_topic'

    while not is_active.is_set():
        for msg in consumer:
            print(msg)

    consumer.close()


if __name__ == '__main__':
    _active = Event()
    try:
        while not _active.is_set():
            kafka_subscriber(_active)
    except KeyboardInterrupt:
        _active.set()
