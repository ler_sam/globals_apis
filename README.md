# Python Supports

## Python Packaging

- [Packaging and distributing projects](https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/)

## setup.py

- __building command:__ ```python setup.py build bdist sdist bdist_wheel```
- __install command:__ ```python setup.py install```
- __upload to private repository:__ ```python setup.py sdist upload -r private-repository```

Configurate PyPi private repository - [jfrog](https://www.jfrog.com/confluence/display/JFROG/PyPI+Repositories)

## Wheels

[Python Wheels](https://pythonwheels.com/)

### Advantages of wheels

1. Faster installation for pure Python and native C extension packages.
2. Avoids arbitrary code execution for installation. (Avoids setup.py)
3. Installation of a C extension does not require a compiler on Linux, Windows or macOS.
4. Allows better caching for testing and continuous integration.
5. Creates .pyc files as part of installation to ensure they match the Python interpreter used.
6. More consistent installations across platforms and machines.

```pip install wheel```

---

# Support Packages

## Convert speech to text

### Modoule [Whisper](https://github.com/openai/whisper)

Whisper is a general-purpose speech recognition model. It is trained on a large dataset of diverse audio and is also a
multi-task model that can perform multilingual speech recognition as well as speech translation and language
identification.

* __Setup__ module

```commandline
pip install git+https://github.com/openai/whisper.git 
```

[openai whisper](https://github.com/openai/whisper)

__YouTube Demo__

[OpenAI Whisper Demo: Convert Speech to Text in Python](https://www.youtube.com/watch?v=HbY51mVKrcE)

[Open AI’s Whisper is Amazing!](https://www.youtube.com/watch?v=OCBZtgQGt1I)

---

# Dynamic logger Implementation

[application_logger](globals_apis/application/)

## Configuration

* logger settings file [settings.py](globals_apis/application/settings.py)
* python code setting example [configuration.py](globals_apis/application/configuration.py)

```commandline
import logging.config
from .configuration import LOGGER_SETTINGS

logging.config.dictConfig (LOGGER_SETTINGS)
# logger = logging.getLogger('automation')
logger = logging.getLogger('production')
```

## usage

```commandline
from support.app_logger import logger

logger.info("--- Generic ---")
logger.debug("--- Generic ---")
logger.warning("--- Generic ---")
logger.error("--- Generic ---")
logger.exception("--- Generic ---")
```

