FLASK_APP=app.py flask shell
from project.users.models import User
user = User(username='test1', email='test1@example.com')
db.session.add(user)
db.session.commit()

User.query.all()
User.query.first().username

[Background Tasks with Celery](https://flask.palletsprojects.com/en/3.0.x/patterns/celery/)

[Asynchronous Tasks with Flask and Celery](https://testdriven.io/blog/flask-and-celery/)

https://testdriven.io/courses/flask-celery/app-factory/#H-0-objectives
celery -A app.celery worker --loglevel=info
celery -A app.celery worker --loglevel=debug
-----------------------------------------------------
```bash
FLASK_APP=app.py flask shell
```
```python
from project.users.tasks import divide
task = divide.delay(1, 2)
```
