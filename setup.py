#!/usr/bin/env python
import os
from setuptools import setup, find_packages

exclude_pkg = ['globals_apis.duplicate_similar_images']

_requires = ['gTTS', 'pytube', 'whisper', 'kafka-python']
if os.name == 'nt':
    _requires.append('win10toast')
else:
    exclude_pkg.append('globals_apis.windows_notification')

setup(
    name='globals_apis',
    version='1.0.0',
    description='General functions support',
    url='https://bitbucket.org/ler_sam/globals_apis/src/master/',
    author='Samuel Lerner',
    author_email='LerSam@gmail.com',
    license='MIT',
    packages=find_packages(include=['globals_apis', 'globals_apis.*'],
                           exclude=exclude_pkg),
    install_requires=_requires,
    dependency_links=['git+https://github.com/openai/whisper.git#egg=whisper-0.1'],

)
