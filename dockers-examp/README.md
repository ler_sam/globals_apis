[TOC]

[DockerHub](https://hub.docker.com/)

- Docker is a software platform that allows you to build, test, and deploy applications quickly.

- Docker makes it easy to develop, deploy, and manage applications by using containers. Container refers to a
  lightweight, stand-alone, executable package of a piece of software that contains all the libraries, configuration
  files, dependencies, and other necessary parts to operate the application.

- In other words, applications run the same irrespective of where they are and what machine they are running on because
  the container provides the environment throughout the software development life cycle of the application. Since
  containers are isolated, they provide security, thus allowing multiple containers to run simultaneously on the given
  host. Also, containers are lightweight because they do not require an extra load of a hypervisor. A hypervisor is a
  guest operating system like VMWare or VirtualBox, but instead, containers run directly within the host’s machine
  kernel.

The default docker images will show all top level images, their repository and tags, and their size.

# [Docker Image](https://docs.docker.com/reference/cli/docker/image/)

**Manage images.**

| Command                          | Description                                                              |
|:---------------------------------|:-------------------------------------------------------------------------|
| <code> docker image ls </code>   | List images                                                              |
| <code> docker image pull </code> | Download an image from a registry                                        |
| <code> docker image push </code> | Upload an image to a registry                                            |
| <code> docker image load</code>  | Load an image from a tar archive or STDIN                                |
| <code> docker image save</code>  | Save one or more images to a tar archive (streamed to STDOUT by default) |
| <code> docker image rm</code>    | Remove one or more images                                                |

**Examples**
pulling docker from remote repository
```commandline
docker pull python:3.10-slim
```

saving docker to disk
```commandline
docker save -o python-310.tar python:3.10-slim
```
loading docker from local tar file
```commandline
docker image load -i python-310.tar
```

## [Build Docker Image](https://docs.docker.com/reference/cli/docker/build-legacy/)(!legacy)

**Build Docker image from a Dockerfile**

| Option      | Description                                            |
|:------------|:-------------------------------------------------------|
| --build-arg | Set build-time variables (variable-name= variable-val) |
| -t, --tag   | Name and optionally a tag in the name:tag format       |

```commandline
docker build \
--build-arg USER_NAME="Lerner Samuel" \
-t flask-tutorial:1.0.1 .
```

**[Docker Inspect](https://docs.docker.com/reference/cli/docker/inspect/)** - Return low-level information on Docker
objects

```commandline
docker inspect flask-tutorial:1.0.1 | grep "maintainer"
```
## [Docker Image rm](https://docs.docker.com/reference/cli/docker/image/rm/)
**Remove one or more images**

**Examples**
```commandline
docker rmi flask
```

# Containers
## [Show Containers List](https://docs.docker.com/reference/cli/docker/container/ls/)

| Option       | Description                                      |
|:-------------|:-------------------------------------------------|
| -a, --all    | Show all containers (default shows just running) |
| -q, --quiet  | Only display container IDs                       |
| -f, --filter | Filter output based on conditions provided       |

```commandline
docker ps -a
```

**find out containers id's with status conditions `exited`**

```commandline
docker ps -q --filter "status=exited"
```

## [Docker Container Run ](https://docs.docker.com/reference/cli/docker/container/run/)

**Command runs a new container, pulling the image if needed and starting the container.**

You can restart a stopped container with all its previous changes intact using ``docker start``.

Use ``docker ps -a`` to view a list of all containers, including those that are stopped.

| Option            | Description                                                                           |
|:------------------|:--------------------------------------------------------------------------------------|
| -a, --attach      | Attach to STDIN, STDOUT or STDERR                                                     |
| -d, --detach      | Run container in background and print container ID                                    |
| -e, --env         | Set environment variables                                                             |
| -i, --interactive | Keep STDIN open even if not attached                                                  |
| -t, --tty         | Allocate a pseudo-TTY                                                                 |
| -l, --label       | Set meta data on a container                                                          |
| -m, --memory      | Memory limit                                                                          |
| -v, --volume      | Bind mount a volume                                                                   |
| --mount           | Attach a filesystem mount to the container                                            |
| --name            | Assign a name to the container                                                        |
| --network         | Connect a container to a network                                                      |
| --rm              | Automatically remove the container and its associated anonymous volumes when it exits |
| -w, --workdir     | Working directory inside the container                                                |

### With bash shell

```commandline
docker run -it --rm \
flask-tutorial:1.0.1 bash
```

- running flask-tutorial:1.0.1 image with bash command-line
- `-it` - connecting your terminal to the I/O streams of the container
- `--rm` - will remove container on the end

### With network mapping

background docker run with mapped network

connecting Computer and Docker to run on same network

```commandline
docker run -d --rm \
--network host \
--name flask \
flask-tutorial:1.0.1
```

### With mount directory and bash shell

can be used to debug, copy and manipulate files on runtime

```commandline
docker run -it --rm \
--network host \
--name flask \
-v "$(pwd)/app":/app \
flask-tutorial:1.0.1 bash
```

```commandline
docker run -it --rm\
--network host \
--name flask \
--mount src="$(pwd)/app",target=/app,type=bind \
flask-tutorial:1.0.1 bash
```

## [Docker Container Logs](https://docs.docker.com/reference/cli/docker/container/logs/)

**Command batch-retrieves logs present at the time of execution.**

| Option       | Description                                      |
|:-------------|:-------------------------------------------------|
| -f, --follow | Follow log output                                |
| -n, --tail   | Number of lines to show from the end of the logs |

**Examples**
```commandline
 docker logs -f flask
```
show last 10 line of flask container

```commandline
docker logs -n 10 flask
```
## [Docker Container Stop](https://docs.docker.com/reference/cli/docker/container/stop/)
**Stop one or more running containers**

**Examples**

```commandline
docker stop flask
```
## [Docker Container rm](https://docs.docker.com/reference/cli/docker/container/rm/)
**Remove one or more containers**

**Examples**
```commandline
docker rm flask
```